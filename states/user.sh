
# Specific user files
# ===============================================

TrackFile '/etc/machine-id'
TrackFile '/etc/hostname'
TrackFile '/etc/hosts'
TrackFile '/etc/passwd'
TrackFile '/etc/shadow'
TrackFile '/etc/group'
TrackFile '/etc/gshadow'
TrackFile '/etc/mtab'
TrackFile '/etc/fstab'

TrackFile '/etc/crypttab'
TrackFile '/etc/crypttab.initramfs'
TrackFile '/etc/default/grub'
TrackFile '/etc/exports'
TrackFile '/etc/mkinitcpio.conf'
TrackFile '/etc/shells'
TrackFile '/etc/sudoers'
TrackFile '/etc/vconsole.conf'
TrackFile '/etc/nfs.conf'

TrackFile '/etc/pacman-mirrors.conf'
TrackFile '/etc/systemd/*'
TrackFile '/etc/fonts/fonts.conf'
TrackFile '/etc/NetworkManager/system-connections/*'


# Setup
TrackFile '/etc/mkinitcpio.d/*'

#Others
TrackFile '/etc/pacman.d/mirrorlist.pacnew'
TrackFile '/etc/adjtime'
TrackFile '/etc/locale.conf'
TrackFile '/etc/locale.gen'
TrackFile '/etc/pacman.conf'
TrackFile '/etc/pacman.d/mirrorlist'
IgnorePath '/etc/localtime' # BUG: Must create TrackLink

