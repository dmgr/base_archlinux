
# Basic arch provisionners
# ==============================

function SetLocales()
{
  local locale=${1:-$SYS_LOCALE}

  LogEnter 'Configure /etc/locale.conf to %s\n' "$locale"

    # Configure locales and keyboards
    export LANG="$locale"
    $dry_mode || { echo "LANG=$locale" > /etc/locale.conf ; }

    # Configure locales
    Exec sed -i 's/^[^#].*/#\0/' /etc/locale.gen
    Exec sed -i "s/#$locale /$locale /" /etc/locale.gen
    Exec locale-gen

  LogLeave 'Locales has been configured to %s\n' \
    "$(Color G "%s" "$locale" )"
  StateSet SystemLocales $locale
}

function SetHostname()
{
  local hostname=${1:-$SYS_HOSTNAME}
  local domain=${2:-$SYS_DOMAIN}

  LogEnter 'Set hostname and domain to %s\n' \
    "$hostname.$domain"

  Log 'Set /etc/host to %s\n' "$hostname"
  Log 'Set /etc/hosts to %s\n' "$hostname.$domain"
  StateSet SystemHostname $hostname
  StateSet SystemDomain $domain

  # Quit on dry mode
  if $dry_mode; then
    LogLeave
    return
  fi

  # Configure hostname
  echo "$hostname" > /etc/hostname

  # Configure domain
  local config="$hostname.local $hostname"
  if [[ "${domain:-local}" != 'local' ]]; then
    config="$hostname.$domain $hostname.local $hostname"
  fi
  cat <<EOF > /etc/hosts
# Static table lookup for hostnames.
# See hosts(5) for details.

# Base configuration
# ---------------------

# Localhost management
127.0.0.1     localhost
::1           localhost

# Local domain host
127.0.1.1     $config

# User  configuration
# ---------------------

EOF
  LogLeave
}


# User Management
# ==============================

function CreateUser ()
{
  local user=$1
  LogEnter 'Create user account %s\n' "$user"
  if id -u $user &>/dev/null ; then
    LogLeave 'User: User %s is already present\n' "$user"
  else
    Exec useradd $user --create-home \
      --comment "Regular user account"
    LogLeave 'User: Regular account %s created\n' "$user" 
  fi
}

function CreateSysUser ()
{
  local user=$1

  LogEnter 'Create service account %s (var/lib/%s)\n' "$user" "$user"
  if id -u $user &>/dev/null ; then
    LogLeave 'User: User %s is already present\n' "$user"
  else
    Exec useradd $user \
      --system --create-home \
      --home-dir "/var/lib/$user" \
      --comment "Regular service account"
    LogLeave 'User: Service account %s created\n' "$user" 
  fi
}

function CreateSysGroup ()
{
  local group=$1
  if getent group $group &>/dev/null; then
    Log 'Service group %s is already present\n' "$group"
  else
    Log 'Create service group %s\n' "$group"
    Exec groupadd $group --system
  fi
}

function AddUserToGroup()
{
  local user=$1
  local group=$2
  if getent group $group | grep -q "[,:]$user"; then
    Log 'User %s already belong to group %s\n' "$user" "$group"
  else
    Log 'Add user %s to group %s\n' "$user" "$group"
    Exec usermod -G $group $user
  fi
}

function SetUserPass ()
{
  local SYS_PASSWD=

  if [[ -z "${SYS_PASSWD-}" ]]; then
    Log 'Passwd: No password set for %s\n' "$user"
    return 0
  fi
  
  Log 'Passwd: Update %s passwrd\n' "$user"
  if ! $dry_mode; then
    if ! id -u $user &>/dev/null ; then
      Log 'Passwd: Missing user %s, cant change passwd\n' "$user"
      return 3
    fi
    printf "$SYS_PASSWD\n$SYS_PASSWD\n" | passwd $user > /dev/null
  fi

}


# Configure Pacman
# ==============================

function ConfigurePacmanMirror()
{
  local country=${1:-$SYS_COUNTRY}
  local mirror_conf="/etc/pacman.d/mirrorlist"

  if [[ -z "$(sed -n '/^[^#].*/p' "$mirror_conf")" ]]; then

    Log 'Rank online mirrors for country %s ...\n' "$country"
    local mirrors=$(curl -s "https://www.archlinux.org/mirrorlist/?country=$country&country=US&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d')

    # Detect fastest mirror
    if ! $dry_mode && command -v rankmirrors 2>/dev/null >/dev/null ; then
      echo "$mirrors" | rankmirrors -n 5 - >> "$mirror_conf"
    elif ! $dry_mode; then
      Log 'Missing rankmirrors, using first line repo\n'
      echo "$mirrors" | grep -m 1 "Server" >> "$mirror_conf"
    fi

    Log 'Mirror has been configured in %s\n' \
      "$(Color Y "%s" "$mirror_conf" )" 
  fi

  StateSet SystemPacmanMirror true
}

function ConfigurePacman()
{
  ConfigurePacmanMirror

  LogEnter 'Configure pacman keys and update sources\n'
    StateRequire SystemPacmanMirror
    Exec pacman-key --init
    Exec pacman-key --populate archlinux
    Exec pacman -Sy  
  LogLeave 'Pacman is configured\n'
  StateSet SystemPacman true
}


# Boot configuration
# ==============================


function InstallKernel()
{
  LogEnter 'Install Linux kernel packages\n'
    StateRequire SystemPacman true
    Exec pacman -S --noconfirm --needed mkinitcpio linux linux-firmware
    StateSet SystemKernel linux
  LogLeave
}

function InstallGrub()
{
  local part=${1:-false}

  LogEnter 'Install Grub bootloader packages\n'

    # Requirements
    StateRequire SystemPacman true
    StateRequire SystemKernel linux

    # Install packages
    Exec pacman -S --noconfirm --needed grub efibootmgr dosfstools

    # Install Grub on EFI
    if [[ "$part" == false ]]
    then
      Log 'Install Grub without EFI (no partition configured)\n'
      Exec grub-install \
        --target=x86_64-efi \
        --boot-directory=/boot \
        --no-nvram
      LogLeave 'Bootloader correctly installed (without EFI)\n'
    else
      Log 'Install Grub and EFI (EFI disk %s)\n' "$part"

      # Install grub with EFI
      if ! mountpoint /boot/efi/ 2>/dev/null; then
        {
          Exec mkdir -p /boot/efi/ && \
            Exec mount $part /boot/efi && \
              Exec grub-install \
                --target=x86_64-efi \
                --boot-directory=/boot \
                --efi-directory=/boot/efi \
                --bootloader-id="$SYS_DIST"

        } || {
            LogLeave 'Can'"'"'t mount EFI partition system from: $part, skip boot install.\n'
          return 1
          }
      fi
    LogLeave 'Bootloader correctly installed (with EFI)\n'
  fi
  StateSet SystemBoot grub2

}

function ConfigureGrub()
{
  LogEnter 'Configure mkinitcpio and Grub config\n'

    # Requirements
    StateRequire SystemKernel linux
    StateRequire SystemBoot grub2

    # Run the cocnfig commands
    Exec mkinitcpio -P
    Exec grub-mkconfig  -o /boot/grub/grub.cfg

    #Exec umount /boot/efi

  LogLeave 'Bootloader correctly configured\n'
}


# Post config
# ==============================

function ConfigureTime()
{
  local tz="${1:-$SYS_TZ}"

  LogEnter 'Configure System Time (%s)\n' "$tz"

    StateRequire SystemPacman true

    # Time information
    Exec pacman -S --noconfirm --needed util-linux
    Exec ln -sf /usr/share/zoneinfo/$tz /etc/localtime

    # Set motherboardtime
    Exec hwclock --systohc

  LogLeave
  StateSet SystemTime $tz
}


function CreateFstab()
{
  Log 'Configure System /etc/fstab\n'
  StateRequire SystemPacman
  StateRequire SystemBoot

  if ! command -v genfstab 2>/dev/null >/dev/null ; then
    Exec pacman -S --noconfirm --needed arch-install-scripts
  fi

  Log 'Generate /etc/fstab\n'
  if ! $dry_mode; then
    genfstab -U / > /etc/fstab
  fi

  StateSet SystemFstab true
}


# Dist helpers
# ==============================

# Those script must be idempotent

# Helper to magically detect current TZ/Locale/Country
function ArchGetLocation()
{
  local locale=${1-}

  # Skip if already mentionned
  if [[ -n "${SYS_TZ-}" ]] && \
    [[ -n "${SYS_COUNTRY-}" ]] && \
    [[ -n "${SYS_LOCALE-}" ]]
  then
    return
  fi

  # Fetch data
  LogEnter 'Detect country and timezone ...\n'
    local data=$( curl -s ipinfo.io | sed -nE '{s/",$//;s/": "/=/;s/^\s*"/var_/p}' )
    while IFS== read -r key val _ ; do
      val="${val//\$}"
      case "$key" in 
        var_timezone)
          SYS_TZ="${SYS_TZ:-$val}"
          ;;
        var_country)
          SYS_COUNTRY="${SYS_COUNTRY:-${val^^}}"
          ;;
      esac
    done <<< "$data"

    # Set locale
    if [[ -n "${locale-}" ]]; then
      SYS_LOCALE=$local
    else
      SYS_LOCALE="${SYS_LOCALE:-${SYS_COUNTRY,,}_${SYS_COUNTRY}.UTF-8}"
    fi

  LogLeave 'Detected locale is %s, timezone is %s and country is %s\n' \
    "$(Color G "%s" "$SYS_LOCALE" )" \
    "$(Color G "%s" "$SYS_TZ" )" \
    "$(Color G "%s" "$SYS_COUNTRY" )"
}


# Helper function to create initial admin
function ArchAdminAccount ()
{
  local user=${1:-$SYS_USER_ACCOUNT}

  LogEnter 'Ensure there is main admin user\n'

  # Create admin groups
  CreateSysGroup sudo "Sudo with password"
  CreateSysGroup wheel "Sudo without password"

  # Create user
  if $SYS_SERVICE_ACCOUNT && \
    ! StateTest SystemMaintUser "$SYS_USER_ACCOUNT"; then
    CreateSysUser $user
  else
    CreateUser $user
  fi

  # Define password and make it admin
  if [[ -z "${SYS_USER_PASS-}" ]]; then
    SetUserPass $user $SYS_USER_PASS
    AddUserToGroup $user sudo
  else
    AddUserToGroup $user wheel
  fi

  LogLeave 'System user has been created\n'
  StateSet SystemMaintUser "$user"
}


ArchInstallYay ()
{
  StateRequire SystemPacman true
  StateRequire SystemMaintUser true

  LogEnter 'Install yay with %s user\n' "$(StateGet SystemMaintUser)"
  #if ! id -u $(StateGet SystemMaintUser) &>/dev/null ; then
  #  FatalError 'Missing standard user for yay, cant continue. SYS_YAY_USER=%s\n' "$SYS_YAY_USER"
  #  Exit
  #fi
  local SYS_YAY_USER=$(StateGet SystemMaintUser)

  # Try to install with himself
  if command -v yay 2>/dev/null >/dev/null ; then
    Exec /usr/bin/sudo -u $SYS_YAY_USER -- yay -S --noconfirm yay
  else

    # Start manual install
    Exec pacman -S --noconfirm --needed git sudo linux-headers base-devel go
    Log 'Compile and install yay package ...\n'
    if $dry_mode; then return; fi

    if ! [[ -d "/tmp/yay_src" ]]; then
      /usr/bin/sudo -u $SYS_YAY_USER -- bash -c "git clone https://aur.archlinux.org/yay.git /tmp/yay_src && cd /tmp/yay_src && makepkg -si --noconfirm"
    else
      /usr/bin/sudo -u $SYS_YAY_USER -- bash -c "cd /tmp/yay_src && makepkg -i --noconfirm"
    fi
  fi

  LogLeave 'Yay has been installed\n'
  StateSet SystemAurMgr yay
}

ArchInstallAconfmgr ()
{
  LogEnter 'Install Aconfmgr\n'
  StateRequire SystemAurMgr yay

  if ! id -u $SYS_YAY_USER &>/dev/null ; then
    Log 'Missing standard user for yay, cant continue. SYS_YAY_USER=%s\n' "$SYS_YAY_USER"
    return 3
  fi
  Log 'Execute as %s yay --noconfirm -Sy aconfmgr-git\n' "$SYS_YAY_USER"
  if ! $dry_mode ; then
    /usr/bin/sudo -u $SYS_YAY_USER -- yay --noconfirm -Sy aconfmgr-git
  fi
  LogLeave 'Aconfmgr  has been installed\n'
}
