
# Default variables
# =========================================

# Define the system name in bootloader
SYS_DIST=${SYS_DIST:-ArchLinux}

# Provides the EFI partition
SYS_EFI_PART=${SYS_EFI_PART:-false}

# Define main system hostname
SYS_HOSTNAME=${HOSTNAME:-archlinux}

# Define main system domain
SYS_DOMAIN=${SYS_DOMAIN:-local}

# Define system locale
SYS_LOCALE=${SYS_LOCALE:-en_US.UTF-8}

# Define system timezone
#SYS_TZ=${SYS_TZ:-Europe/Paris}

# Define system country
#SYS_COUNTRY=${SYS_COUNTRY-}

# Principal user config
SYS_SERVICE_ACCOUNT=${SYS_SERVICE_ACCOUNT:-true}
SYS_USER_ACCOUNT=${SYS_USER_ACCOUNT:-${LOGNAME:-sys_maint}}
SYS_USER_PASS=${SYS_USER_PASS-}

