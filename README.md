# Base Archlinux

A ditribution for aconfmgr2.

### Features

This distribution provides:

* Define core files to manage and ignore
* Manage File Hierarchy
* Install Kernel (Linux)
* Install Bootloader (Grub)
* Standard library to work with Archlinux

A base library:
* Configure Hostname
* Configure Locale
* Configure Time
* Configure fstab
* Configure Pacman

### Function librairies

You can use any of theses function in your setup environments.
```
function SetLocales()
function SetHostname()
function CreateUser ()
function CreateSysUser ()
function CreateSysGroup ()
function AddUserToGroup()
function SetUserPass ()
function ConfigurePacmanMirror()
function ConfigurePacman()
function InstallKernel()
function InstallGrub()
function ConfigureGrub()
function ConfigureTime()
function CreateFstab()
function ArchGetLocation()
function ArchAdminAccount ()

ArchInstallYay ()
ArchInstallAconfmgr ()
```

