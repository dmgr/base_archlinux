
# Load Arch librairies
Load lib arch

# Guess environment
ArchGetLocation

# Configure base system
LogEnter 'Provision base system\n'
  SetLocales $SYS_LOCALE
  SetHostname $SYS_HOSTNAME $SYS_DOMAIN
  ConfigurePacman $SYS_TZ
  ConfigureTime $SYS_TZ
LogLeave 'Base system provisionned\n'

# Configure pacman
LogEnter 'Install Linux and Grub2\n'
  InstallKernel
  InstallGrub "${SYS_EFI_PART-}"
  ConfigureGrub
LogLeave 'Linux and Grub2 have been installed\n'
 
# Files are then applied via ApplyState as it has not been called yet.
